## check-if-host-is-alive v1.0

Bash script which checks whether a host is alive or not, if a port is open or closed, logs it and sends emails accordingly to notify you.

---

## Introduction

check-if-host-is-alive is a linux script(bash) able to help whoever needs to check periodically if a host is up and running, or if a port is open.

**Prerequisites**

The script assumes you have a mail server running on the host machine, running under /usr/bin/mail.

---

## Parameters which need to be set according to your needs

1. logPath (the path in which you wish to save the log file from the execution)
2. HOSTNAME (a list of hostnames to be checked seperated with white spaces)
3. IP (IP address to be used in nmap for port scanning)
4. PORT (port to be checked along with the IP given)
4. SUBJECT (subject for the notification emails to be sent)
5. EMAILID (receiver of the notification emails)

---

## How it works

1. The log file is kept in a daily basis, so the first thing to check is if the log file for that day is already created. If not, it will be.
2. The script checks if it can contact google in order to ensure general connectivity to internet. If the general connectivity check fails, the script terminates
3. The next step is to check the connectivity to every host under HOSTNAME. 
4. If the host is up, nothing is logged, no mail is sent.
5. If the host is down, and no entry for that timestamp has been written to log (error_exists == 0), a new timestamp is added to the log, along with an error message. Also an email is sent to the given address with the given subject.
6. If the host is down, but an entry for the current timestamp is already added (error_exists == 1), then only the error message is written on the logfile, and the email is sent.
7. After the host checking, nmap is used to check connectivity to specific IP and port. The error logging/notification remains the same as above.
8. In some cases you need to check that not only the port is open, but also the server behind is up. For that reason command "curl" is used to retrieve web page behind that port and IP. The error logging/notification remains the same as above.