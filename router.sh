#!/bin/bash

    # Check if a new month/year has started
    day=$(date +"%d")
    now_date=$(date +"%m_%Y")
    error_exists=0 # this variable is used in order to write the date an error occured only once
    logPath="<PATH-TO-LOGS-FOLDER>"

    if [ -f "$logPath/logging_$now_date.txt" ]; then
      echo "File found."
    else
      touch "$logPath/logging_$now_date.txt"
    fi

    # ips / hostnames to be checked, seperated by space
    HOSTNAME="some.host.country" "www.saycheese.pr"
    IP="192.192.192.1"
    PORT="8698"
    # Google hostname is used in order to check your general connectivity to internet
    GOOGLE="www.google.com"

    # no of ping requests
    COUNT=3

    # email report when
    SUBJECT="Ping failed"
    EMAILID="<EMAIL-TO-RECEIVE-FAILURE-NOTIFICATIONS>"

    # Check connectivity to internet
    count=$(ping -c $COUNT $GOOGLE | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
    if [[ "$count" -ne 0 ]]; then

        for myHost in $HOSTNAME
        do
          count=$(ping -c $COUNT $myHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
          if [[ "$count" -eq 0 ]]; then

         # check if date is written
            if [ "$error_exists" == 0 ]; then
         # print date to file
              date >> "$logPath/logging_$now_date.txt"
              error_exists=1
            fi

        # 100% failed
            echo "host $myHost is Dead"
            echo "Host : $myHost is down (ping failed) at $(date)" | /usr/bin/mail -s"$SUBJECT" $EMAILID
            echo "Host : $myHost was down (ping failed)" >> "$logPath/logging_$now_date.txt"    
          else
            echo "host $myHost is good"            
          fi
        done

        state=$(nmap -p $PORT -sT $IP | grep -c 'open')
        if [[ "$state" -eq 0 ]]; then
        # check if date is written
          if [ "$error_exists" == 0 ]; then
          # print date to file
            date >> "$logPath/logging_$now_date.txt"
            error_exists=1
          fi

        # 100% failed
          echo "IP $IP is Dead"
          echo "Host : $IP on port $PORT is down (ping failed) at $(date)" | /usr/bin/mail -s"$SUBJECT" $EMAILID    
          echo "Host : $IP on port $PORT was down (ping failed)" >> "$logPath/logging_$now_date.txt"
        else
          http_status="$(curl -IL --silent http://$IP:$PORT/stream | grep HTTP )";
          #echo "$http_status"
          if [[ $http_status =~ .*200.* ]]; then
             echo "http status is good"
             echo "IP $IP is good"
          else
          # check if date is written
             if [ "$error_exists" == 0 ]; then
             # print date to file
                date >> "$logPath/logging_$now_date.txt"
                error_exists=1
             fi

             # 100% failed
             echo "http on $IP is Bad"
             echo "Host : $IP on port $PORT is up, but /stream is not working at $(date)" | /usr/bin/mail -s"$SUBJECT" $EMAILID    
             echo "Host : $IP on port $PORT was up, but /stream was not working" >> "$logPath/logging_$now_date.txt"
          fi

        fi
        
        # check if date is written
        if [ "$error_exists" == 1 ]; then
        # print empty line to file
          echo -en "\n" >> "$logPath/logging_$now_date.txt"
        fi
    fi
